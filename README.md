__README.md__ 

* clone the repository:                                                           
> git clone https://mark_rudolph@github.com/josefK128/backpropagation.git

  
* install python 3.X (current latest 3.8) - see 
  https://www.python.org/downloads/ - simply click download button and follow 
  defaults for installation
  
* install numpy:  > pip install numpy
* install copy:  > pip install copy

* rnn.py usage:  > py rnn.py (>log)
  This command will write out all diagnostics to the console (or to a logfile)

  The learned task is associating a bit-wise {0,1} sum to two bitwise numbers-
  
example:
Error:[0.47477457]
Pred:[0 0 1 1 1 0 0 0]
True:[0 0 1 1 1 0 0 0]
39 + 17 = 56
